	* https://jitsi.dc21.debconf.org/1-autopkgtest-office-hours-U2JAilzNKwjGtmKY
	* 
	* quick intro
	* goals: help people implementing, or improving, autopkgtest support for their package
	* NEWS since last yeat
		* added several new architectures
			* almost all release architectures
				* amd64, i386, arm64, armhf, ppc64el fully supported
				* s390x present, but not enough power
				* missing mips*
				* armel WIP
		* Pavit's GSoC internship (mid-2021)
			* salsa logins
			* private jobs
			* custom APT repositories (needs a bit more work)
	* repository created for long-term discussion and archiving:
		* https://salsa.debian.org/ci-team/autopkgtest-help/
	* questions (add yours):
		* zigo: Autopkgtest for all packages in the OpenStack team (#1)
			* https://salsa.debian.org/ci-team/autopkgtest-help/-/issues/1
			* python package with existing buid-time tests; usually easy to help
			* if you want to help, please say so in the issue and get in tough with zigo
		* josch: autopkgtest for my package plakativ (#2)
			* https://salsa.debian.org/ci-team/autopkgtest-help/-/issues/2
		* lindi: autopkgtest for tboot (must run on real hardware) (#3)
			* https://salsa.debian.org/ci-team/autopkgtest-help/-/issues/3
			* lava: https://validation.linaro.org/
		* guilhem: DEP-8 tests for cryptsetup
			* https://salsa.debian.org/ci-team/autopkgtest-help/-/issues/4
			* maybe we should have some infrastructure to let autopkgtest control a nested VM for early boot tests
		* gratuxri: I have mips machine, how we can bring it to autopkgtest?
			* talk to {terceiro,elbrus}@debian.org or debian-ci@lists.debian.org if you want to talk about it in public
		* henrich (+1): Is there any tutorial docs for autopkgtest newbies?
			* Start with https://ci.debian.net/doc/file.TUTORIAL.html
		* List of good examples to get inspirations from
			* https://salsa.debian.org/ci-team/autopkgtest-help/-/issues/5 << link is empty list...
		* autopkgtest for nbd, which needs a client and a server if the root-on-NBD support is going to be tested (could be with a VM) (would be handled with expect script/VM running infra)

I'm absolutely new to autopkgtest. I maintain irssi and wesnoth. Is there ways to support tools that require network access (a local running IRC server would work), are interactive and/or use graphics?



