Talk2-Meus passos utilizando Linux              
    
Questions/Pergutas:

Q: Qual a maior dificuldade que você encontrou ao começar a contribuir?
	Bem, acho que a maior dificuldade foi escolher uma área para contribuir já que havia várias com as quais eu me interessava.
	
Q: Você já contribui em várias áreas (tradução, freedombox, etc). Quais foram as melhores experiências e o que acha que podemos melhorar?
	Sem dúvida, foi a receptividade que eu tive em todas as áreas. Eu sempre fui muito bem recebido, todos tinham paciência e me respondiam de forma clara e completa. Não tive experiências ruins, então não tenho sugestões para melhorar.
	
Q.: vc usa KDE ou Gnome? hahaha
Parabéns pelas grandes colaborações! De grão em grão o Debian atinge mais pessoas!
	Eu uso Gnome atualmente. Anteriormente usava o lxde em uma máquina mais antiga e limitada.
