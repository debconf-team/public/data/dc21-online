Building virtual machines with KVM, QEMU and LibVirt

Please leave your questions and comments here, and I will do my best to answer them on the Q&A :D

"Técnico" means coach in portuguese

QUESTION 1

Virtualbox has a very nice GUI and very easy to use. Do we have any such GUIs on Debian for KVM?
A: Debian provides one such GUI through the virt-manager package.  :)

We will see two different GUIs: Gnome Boxes and Virt Manager, both work in any Debian

COMMENT
I find the rant about cryptography and privacy interesting but off-topic for a talk about virtualization. Feel kind of cheated.

I understand.... but we will back to VMs in a moment

I wanted to bring a broader overview for security, but perhaps I overdid it :(
    
It's actually "gnome-boxes --checks" typo to be corrected

Comment: libre firmware is a nice goal, but as seen with by firmware-sof-signed (but probably in other areas as well), there might be a problem with hardware enforcing fixed set of cryptographic keys.

Comment (minor nitpick): one technicality about Mac address is that there are no duplicates on a same network.

Thanks for your talk
