Welcome to DebConf Video Etherpad!

This pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!

To get started click the 👥 button in the top/bottom right and log in.

Get involved with Etherpad at https://etherpad.org

Q: As packager, is there some special care one can -or has to- implement, for programs which might have been "certified for clinical diagnostics", or similar demanding situation?

A: We can hardly pass some certifications.  We just do the packaging and some kind of service company could pick up our packages and provide support for the packages, certifications, etc.
Its not possible for volunteers to do this.

Q: I want to package something for the medicine team, but something that isn't R-related, but instead something small that is super helpful, but I can't seem to find any packages in the WNPP or in general on GitHub that could help. Where does the med team get most of their programs to package?
  -> May I ask for the actual software?  Some URL?

A: Please contact debian-med@lists.debian.org
Alternatively contact https://app.element.io/#/room/#debian-med:matrix.org
We are extremely happy about any new contributor.
We will guide you kindly where to put your packages.
The easisest and smoothest way to start packaging is joining the Mentoring of the Month program:
       https://salsa.debian.org/med-team/community/MoM/-/wikis/home

IMO, rather than grey-shading the "Alioth to Salsa" migration (I don't feel it as determinant), an3as should have highlighted the last ~18 months, as he drove quite a bit of strength into the team since the beginning of the pandemic

I'll keep that in mind for the next chance to talk about this.
My motivation to shade Alioth migration was to mark peaks that were not interesting content-wise for the team.

