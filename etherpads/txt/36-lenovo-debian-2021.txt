Welcome to DebConf Video Etherpad!

This pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!

To get started click the 👥 button in the top/bottom right and log in.

Get involved with Etherpad at https://etherpad.org

-----------------------------------------------------------------------------------

			Questions, re. newer TigerLake (11th Gen) Thinkpads
    
1. How to make hibernation work? :) S0ix drains the battery passionately.
     Given the wakeup time, it looks like laptops in question are not
     reaching the desired S0 state. S3 is massively better in conserving
     energy but causes laptop to force-reboot after the wake-up [1].

[1] https://bugzilla.kernel.org/show_bug.cgi?id=201761#c52

2. There are plenty of various erros in `dmesg` that are platform-specifc.
     Is there someone working on these?

3. There seems to be something off with the I219-LM in newer Thinkpads.
     Ingress traffic is littered with checksum errors. Haven't investigated
     this much as laptops are used via WiFi most of the time.

4. (Question to Intel) When can we expect AX210 support in the 5.10.XX?
     "Proper" meaning one that does not require manual intervention within
     /lib/firmware.

5. What's the Lenovo strategy on making newer SOF / DMICs / D-mixing in
     general operational with LTS kernels? Will you push necessary
     packages into `-backports`?

-----------------------------------------------------------------------------------

Any updates on availability of other models with Linux?  I've been
thinking about X1 Extreme, but would be interested in the options.
(I'm typing this from an X1 Extreme Gen3 running Debian Bullseye :D -Tianon)


 * Is Coreboot/Libreboot discussed at Lenovo? 
 * Does Lenovo consider other levels of support for Coreboot? 
 * Does Lenovo consider to System76/Chromebook-like support it? 

* What is the current status of support for the fingerprint reader in T-470? (in Archlinux :)
(It needs a windows machine to enroll the fingerprint)

* Using a dock station is simply great. Worked pretty much out of the box, but there seem to be some issues around sound cards, mic and whatnot (which could be rather linked to pulseaudio) - when we plug it out
I use two monitors, besides the notebook screen itself
Jitsi is a no-go on the dock, not sure why

I am wondering if Debian or Debian based linux distros support my microphone on my Lenovo Thinkpad L380 yoga because when i run Debian or Debian based Distro on my thinkpad my mic is distroted and you can hear a loud background noise that is not really there? For some reason issue does not happen on OpenSUSE or Suse Linux Enterprise Desktop. But happens on all other linux distros including Ubuntu and Fedora?
I saw it was ubuntu certified on lenovo? The issue even happens on Ubuntu. It has an intel core i7 8th gen. 


There is any expectation when we in Brazil will could use the Lenovo discount?

Maybe I'm wrong but as far as I remember in the last year's presentation there were some asking of contribution. How I/we can help increasing the Debian coverage on Lenovo laptops?

Any chances Lenovo would adopt some of Framework's practices and make accessing maintenance manuals and replacement parts easier? Maybe come up with a easy to maintain/upgrade platform? (not really Debian-related but still, sometimes this pops up as an issue especially where parts are harder to find)

Framework is a Canadian company that makes laptops that are easy to open, maintain and upgrade, with easily accessible manuals and part specifications. https://frame.work/

How well do these computers work without non-free or contrib repositories enabled?

Mark, you're awesome, and you absolutely get to own that "maintainer" title, DD or not. :)

Thunderbolt docks have multiple issues across different distros, but of course, i would like to know how is Lenovo Linux Certification team is working towards solving most of the issues.
high refresh rates on DP are flaky over the TB3 docks (Both of these last questions on behalf of roliverio)

Is it possible that OEM-upstream gap could not be closed?  Like some workarounds on some devices could not be upstreamed.  If so, how, as a Debian user/contributor, could help about this?

Tuxedo Computers offers laptops, which can disable Intel ME from BISO setup. Do you offer something similar?

Is https://www.thinkwiki.org/wiki/ a product of Lenovo?  whois tells me it's registered in Germany
­— No, ThinkWiki is a community-run collection of information regarding ThinkPads.





