Debuginfod on Debian

Q: 
    
    Paul Flaherty: What is the name of the tool to automate the gdb processes?
    A: Answered in the video.

From IRC:
<elbrus> sergiodj: thought about a section about debuginfod in the release notes in the new section? (or was that already there and I forgot)
<sergiodj> elbrus: hm, good idea.  there's nothing about debuginfod there AFAIR

<axhn> Are the downloaded files cached locally for the bandwitdh-impaired?
A: Answered in the video.

Q: is it possible to limit what debug packages are downloaded (so one can skip downloading all the shared libs one doesn't need, eg. low on bandwidth or disk space)? 
A: Try "set auto-solib-add off" before starting the binary.  You will then have to manually load the debuginfo for shlibs by using the "sharedlibrary" command.

Q: It's any possibility to have a local mirror of debuginfod. For working offline?
A: Yes!  debuginfod is federated, so you can have your local mirror without problems.  Bear in mind that you need a lot of storage to store debuginfos, though.
