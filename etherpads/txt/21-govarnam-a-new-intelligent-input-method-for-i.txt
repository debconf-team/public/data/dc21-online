THE PROJECT LINK IS THIS : https://github.com/varnamproject/govarnam
DOWNLOAD Varnam FROM HERE ^

COMMUNITY CHAT Telegram - https://t.me/varnamproject
Matrix - #varnamproject:poddery.com

Question 1: for those not familiar with these languages, can you show the difference between typing with varnam and with whatever people use/used before it?

There are multiple ways. There's a qwerty layout with each key mapped to the Indian characters. This is called . Inscript. But very very few people use it because there is a significant learning curve.

The most prominent use is by Google Input Tools. Varnam is basically a FOSS alternative to input tools. Google Input tools is not available offline, can't learn new words, and is not customizable plus there are privacy issues.

Google has Android implementation with GBoard which is the most popular.

We have a FOSS keyboard Indic Keyboard : https://indic.app
Varnam is implemented in Indic Keyboard. Haven't released yet.
	
Question 2: 
I am wondering if varnam supports other operating systems besides linux and android which is a version of linux?

The library is cross-platform. Can be implemented anywhere. Varnam has been successfully implemented in Indic Keyboard Android app (https://indic.app) which will be released soon.

Can be implemented in Mac & Windows. Need help ! It's just me working on Varnam these days ! :D
PRs welcome :) Contact at the links mentioned above ^^

Question 3: 

