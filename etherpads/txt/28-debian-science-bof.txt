Debian Science BoF

26..08.2021, 17:00 UTC.
https://debconf21.debconf.org/talks/28-debian-science-bof/

Participants:
 - Anton Gladky
 - Andreas Tille
 - Alastair McKinstry
 - Steffen Möller
 - Étienne Mollier
 - Diane Trout
 - Sridhar Gutam
 - Doug Torrance
 - Michael D'Souza
 - aghisla (from Neurodebian)

In an attempt to collect "keywords of concern" to discuss:
    * backports - there is no way we can cope with maintenance work, right?
    * new queue and daisy-chain of dependencies
    * (non-free-)buildd compiling CUDA and ROCm packages
    * Debian-internal promotion of Debian-Science-blend?
      - Not required, is it?
      - Is data science prominent enouigh?
      - From Alastair's presentation: Singularity-based workflows would benefit from some promotion/tutorials
    * Should Debian invest into machines with special hardware that are not yet mainstream?
	    GPUs (maybe a porterbox, and maybe a autopkgtest Requires option for needs GPU for tests?)
	    FPGA?
    *  Link to Debian-Jobs - Is there a point in offering internships? Summer  students? Bachelor / Master thesis projects?
    * ... ?
    
    To move forward we need to find a supplier for a non-free builder for the AI/GPU hardware. Who and where should we ask?
Intel for Movidius / Myriad ?
Xilinx for FPGA ?
NVIDIA for CUDA?
If Debian systems team agree, I may be able to get hardware and rackspace in Ireland (Alastair)

    (for singularity, it would be nice to have a smaller debian base for a container than debootstrap  theres a lot of things unneeded for a container that gets included)
    Can the docker-slim images be an option or as an a base for singularity-images? (Anton)
    Some tools were created for debian-installer to minimize scripts (Alastair)

Do we want to provide a simple `create-container` script?

Meta:  I see lots of people reading the pad.  Feel free to join us at
    https://jitsi.dc21.debconf.org/28-debian-science-bof-2oQYAfbxAvujKs0w
    (specifically the "Read-Only Guests" might like to join directly. ;-) )
    (I found the login in the upper right, and it used salsa authentication also the jitsi link has better video quality than the web stream)

